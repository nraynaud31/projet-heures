import { Application } from "express";
import HoursController from "./controllers/HoursController";

export default function route(app: Application)
{
    /** Static pages for Hours**/
    app.get('/', (req, res) =>
    {
        HoursController.index(req, res);
    });

    app.get('/hours-all', (req, res) =>
    {
        HoursController.index(req, res)
    });

    app.get('/hours-create', (req, res) =>
    {
        HoursController.showForm(req, res)
    });

    app.post('/hours-create', (req, res) =>
    {
        HoursController.create(req, res)
    });

    app.get('/hours-update/:id', (req, res) =>
    {
        HoursController.showFormUpdate(req, res)
    });

    app.post('/hours-update/:id', (req, res) =>
    {
        HoursController.update(req, res)
    });

    app.get('/hours-delete/:id', (req, res) =>
    {
        HoursController.delete(req, res)
    });

    app.post('/delete-all', (req, res) => {
        HoursController.deleteAll(req, res)
    })

    app.get('/delete-all', (req, res) => {
        HoursController.deleteAll(req, res)
    })
}
