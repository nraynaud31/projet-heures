import { Request, Response } from "express-serve-static-core";

export default class HoursController {

    /**
     * Affiche la liste des heures
     * @param req Request
     * @param res Response
     */
    static index(req: Request, res: Response) {
        const db = req.app.locals.db;

        const hours = db.prepare('SELECT * FROM hours').all();                
        const calc_hours_ecole = db.prepare("SELECT SUM(hours_numbers) AS sum_total FROM hours WHERE type = 'ecole'").get()
        const calc_hours_mamie = db.prepare("SELECT SUM(hours_numbers) AS sum_total FROM hours WHERE type = 'mamie'").get()
        res.render('pages/index', {
            title: '',
            hours: hours,
            calc_hours_ecole: calc_hours_ecole,
            calc_hours_mamie: calc_hours_mamie
        });
    }

    /**
     * Affiche le formulaire d'ajout des heures
     * @param req Request
     * @param res Response
     */
    static showForm(req: Request, res: Response): void {
        const db = req.app.locals.db;

        const hours = db.prepare("SELECT * FROM hours").all();

        res.render('pages/project-create', {
            hours: hours
        });
    }

    /**
     * Récupère le formulaire et insere les heures en db
     * @param req Request
     * @param res Response
     */
    static create(req: Request, res: Response): void {
        const db = req.app.locals.db;

        db.prepare('INSERT INTO hours ("type", "date", "hours_numbers") VALUES (?, ?, ?)').run(req.body.type, req.body.title, req.body.content);

        HoursController.index(req, res);
    }

    /**
     * Affiche 1'heure concernée
     * @param req Request
     * @param res Response
     */
    static read(req: Request, res: Response): void {
        const db = req.app.locals.db;

        const hours = db.prepare('SELECT * FROM hours WHERE id = ?').get(req.params.id);

        res.render('pages/project-read', {
            hours: hours
        });
    }

    /**
     * Affiche le formulaire pour modifier une heure
     * @param req Request
     * @param res Response
     */
    static showFormUpdate(req: Request, res: Response) {
        const db = req.app.locals.db;

        const hours = db.prepare('SELECT * FROM hours WHERE id = ?').get(req.params.id);

        res.render('pages/projects-update', {
            hours: hours,
        });
    }

    /**
     * Récupère le formulaire de l'heure modifiée et l'ajoute dans la db
     * @param req Request
     * @param res Response
     */
    static update(req: Request, res: Response) {
        const db = req.app.locals.db;

        db.prepare('UPDATE hours SET type = ?, date = ?, hours_numbers = ? WHERE id = ?').run(req.body.type, req.body.title, req.body.content, req.params.id);

        HoursController.index(req, res);
    }

    /**
     * Supprime une heure
     * @param req Request
     * @param res Response
     */
    static delete(req: Request, res: Response) {
        const db = req.app.locals.db;

        db.prepare('DELETE FROM hours WHERE id = ?').run(req.params.id);

        HoursController.index(req, res);
    }

    /**
     * Supprime toutes les heures renseignées
     * @param req Request
     * @param res Response
     */
    static deleteAll(req: Request, res: Response) {
        const db = req.app.locals.db;

        db.prepare('DELETE FROM hours').run()

        HoursController.index(req, res);
    }
}