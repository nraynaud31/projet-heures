-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        NicolasRAYNAUD
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2022-03-04 14:45
-- Created:       2022-03-04 14:45
PRAGMA foreign_keys = OFF;

-- Schema: mydb
--ATTACH "mydb.sdb" AS "mydb";
BEGIN;
CREATE TABLE "hours"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "type" VARCHAR(45) NOT NULL,
  "date" VARCHAR(45) NOT NULL,
  "hours_numbers" INTEGER NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
COMMIT;
