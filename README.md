# Projet heures

Projet pour ma mère permettant de comptabiliser l'ensemble des heures de travail dans le mois.

## Photo du site

![Image du site de Gestion d'Heures](https://i.ibb.co/Xx7xVb8/Capture-d-cran-de-2022-02-27-00-06-39.png)

## Utilisation

Commandes | Rôles
--- | --- 
`npm i` | Installer les dépendences de Node présentes sur le projet
`npm start` | Pour lancer le serveur de dev (Faut installer les packages node avant)
`npm run tsbuild` | Pour build le projet dans le dossier dist